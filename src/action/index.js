import {
  BASE_URL,
} from '../global'

const handleResponse = (response) => {
  console.log('handleResponse', response);
  return new Promise((resolve, reject) => {
      if (response.ok) {
          response.json().then(json => resolve(json))
      }
      else {
          response.json().then(json => reject(json))
      }
  })
}

const handleError = (error) => {
  console.log('handleError', error);
  return Promise.reject(error?.message)
}

/* Post */

export const loginUser = (email, password) => {
  const requestOptions = {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
      },
      body: JSON.stringify({
          email,
          password,
          device_name: 'test',
          device_token: 'badboy'
      })
  }

  return fetch(
      `${BASE_URL}/login`, requestOptions)
      .then(handleResponse, handleError)
}



