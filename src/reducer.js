export default reducer = (state = {}, action) =>{
  if(action.type === 'userUpdated')
      return {
          ...state,
          currentUser: action.payload.data
      }
  return state
}