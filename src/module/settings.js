import React from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import * as COLOR from '../colour';

const Settings = ({ navigation }) => {

  return (
    <View style={styles.viewRoot}>
      <Text style={styles.textTitle}>Settings</Text>
      <View style={styles.viewItem}>
        <Text style={styles.textLabel}>VoIP ID</Text>
        <Text style={styles.textValue}>call123456789</Text>
      </View>
      <View style={styles.viewItem}>
        <Text style={styles.textLabel}>Overlay Permission</Text>
        <Text style={styles.textValue}>Denied</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    backgroundColor:COLOR.WHITE_HEX, 
    flex:1, 
    paddingHorizontal:25
  },
  viewItem:{
    backgroundColor:COLOR.GREY_EEE_HEX, 
    padding:15, 
    borderRadius:10, 
    marginBottom:15
  },
  textTitle:{
    fontWeight:'bold', 
    fontSize:24, 
    marginBottom:30
  },
  textLabel:{
    fontSize:16, 
    fontWeight:'bold', 
    marginBottom:5
  },
  textValue:{
    fontSize:15
  }
});

export default Settings;