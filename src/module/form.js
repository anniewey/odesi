import React from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';
import * as COLOR from '../colour';

const Form = ({ navigation }) => {

  return (
    <View style={styles.viewRoot}>
      <Text style={styles.textTitle}>Form</Text>
      <Text style={styles.textValue}>No pending form</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    backgroundColor:COLOR.WHITE_HEX, 
    flex:1, 
    paddingHorizontal:25
  },
  textTitle:{
    fontWeight:'bold', 
    fontSize:24, 
    marginBottom:30
  },
  textValue:{
    fontSize:15
  }
});

export default Form;