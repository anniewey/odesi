import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as COLOR from '../colour';

const IMG_COMMUNITY = require('../../assets/image/community.png')
const IMG_VISITOR = require('../../assets/image/visitor.png')
const IMG_FEEDBACK = require('../../assets/image/feedback.png')
const IMG_BOOKING = require('../../assets/image/booking.png')
const IMG_FORMS = require('../../assets/image/forms.png')
const IMG_MANAGEMENT = require('../../assets/image/management.png')
const IMG_REFERRAL = require('../../assets/image/referral.png')
const IMG_BILLING = require('../../assets/image/billing.png')
const IMG_MYHOUSE = require('../../assets/image/myhouse.png')

const features=[
  {
    name: 'Community',
    image: IMG_COMMUNITY
  },
  {
    name: 'Visitor',
    image: IMG_VISITOR
  },
  {
    name: 'Feedback',
    image: IMG_FEEDBACK
  },
  {
    name: 'Booking',
    image: IMG_BOOKING
  },
  {
    name: 'Forms',
    image: IMG_FORMS
  },
  {
    name: 'Management',
    image: IMG_MANAGEMENT
  },
  {
    name: 'Referral',
    image: IMG_REFERRAL
  },
  {
    name: 'Billing',
    image: IMG_BILLING
  },
]

const Home = ({ navigation }) => {
  const [userName, setUserName] = useState('')

  const userData = useSelector(state=> state.currentUser)

  useEffect(() => {
    if(userData) setUserName(userData.full_name)
  }, [])

  const renderFeature = ({item}) => {
    return(
      <TouchableOpacity>
        <View style={{alignItems:'center'}}>
          <Image source={item.image} style={{width:70, height:70}}/>
          <Text style={{marginTop:5, color:COLOR.GREY_444_HEX}}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  return (
    <View style={styles.viewRoot}>

      <View style={styles.viewGreeting}>
        <View>
          <Text style={styles.textGreeting}>Hello</Text>
          <Text style={styles.textUserName}>{userName}</Text>
        </View>
        <TouchableOpacity>
          <>
            <Icon name='bell' size={35} style={styles.iconBell}/>
            <View style={styles.viewAlert}/>
          </>
        </TouchableOpacity>
      </View>

      <TouchableOpacity>
        <View style={styles.viewHouse}>
          <View>
            <Text style={styles.textHouse}>My House</Text>
            <Text style={styles.textHouseNote}>Visit your house now</Text>
          </View>
          <Image source={IMG_MYHOUSE} style={styles.imgHouse}/>
        </View>
      </TouchableOpacity>

      <FlatList 
        data={features}
        renderItem={renderFeature}
        numColumns={4}
        style={styles.listModule}
        columnWrapperStyle={styles.listColModule} 
        ItemSeparatorComponent={()=>(<View style={styles.viewSeparator}/>)}
        keyExtractor={(item, index) => index.toString()}
        scrollEnabled={false}
      />

      <TouchableOpacity style={styles.btnSos}>
        <View style={styles.viewSos}>
          <Icon name='alarm-light' size={35} style={styles.iconSos}/>
          <Text style={styles.textSos}>SOS</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  viewRoot:{
    flex:1,
    backgroundColor:COLOR.WHITE_HEX, 
    paddingHorizontal:25
  },
  viewGreeting:{
    flexDirection:'row', 
    alignItems:'center', 
    justifyContent:'space-between', 
    marginBottom:30
  },
  viewAlert:{
    width:10, 
    height:10, 
    backgroundColor:COLOR.RED_HEX, 
    position:'absolute', 
    top:2, 
    right:5, 
    borderRadius:5
  },
  viewHouse:{
    padding:15, 
    backgroundColor:COLOR.GREY_EEE_HEX, 
    borderRadius:15, 
    marginBottom:30, 
    flexDirection:'row', 
    justifyContent:'space-between', 
    alignItems:'center'
  },
  viewSeparator:{
    height:20
  },
  viewSos:{
    padding:15, 
    backgroundColor:COLOR.THEME_HEX, 
    borderRadius:40, 
    width:80, 
    height:80, 
    alignItems:'center', 
    justifyContent:'center'
  },
  textGreeting:{
    fontSize:18, 
    color:COLOR.GREY_999_HEX
  },
  textUserName:{
    fontWeight:'bold', 
    fontSize:24
  },
  textHouse:{
    fontWeight:'bold', 
    fontSize:18
  },
  textHouseNote:{
    marginTop:10
  },
  textSos:{
    marginTop:5, 
    fontWeight:'bold', 
    color:COLOR.WHITE_HEX
  },
  iconBell:{
    color:COLOR.GREY_CCC_HEX
  },
  iconSos:{
    color:COLOR.WHITE_HEX
  },
  imgHouse:{
    width:50, 
    height:50
  },
  listModule:{
    paddingHorizontal:5
  },
  listColModule:{
    justifyContent: 'space-between'
  },
  btnSos:{
    position:'absolute', 
    right:20, 
    bottom:20, 
    shadowOffset: { height: 1, width: 1 }, 
    shadowOpacity: 0.4
  }
});

export default Home;